package eu.primes.chat.internal;

import java.awt.Color;
import java.awt.Paint;
import java.util.ArrayList;
import java.util.Set;

import org.cytoscape.app.swing.CySwingAppAdapter;
import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.view.layout.CyLayoutAlgorithm;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.view.presentation.property.NodeShapeVisualProperty;
import org.cytoscape.view.presentation.property.values.NodeShape;
import org.cytoscape.view.vizmap.VisualMappingFunctionFactory;
import org.cytoscape.view.vizmap.VisualStyle;
import org.cytoscape.view.vizmap.mappings.BoundaryRangeValues;
import org.cytoscape.view.vizmap.mappings.ContinuousMapping;
import org.cytoscape.view.vizmap.mappings.DiscreteMapping;
import org.cytoscape.view.vizmap.mappings.PassthroughMapping;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.TaskMonitor;

/**
 * This displays the network in Cytoscape, applies a layout and a new visual style by setting node
 * and edge properties.
 * 
 * @author Tanja Muetze, Ivan Goenawan
 */

public class ConstructNetworkViewTask extends AbstractTask{
	
	private CySwingAppAdapter appAdapter;
	private CyActivator activator;
	private CyNetwork network;
	private String[] userDefinedParameters;
	private ArrayList<String> interactionTypesNamesList;
	private CyLayoutAlgorithm algor;
	
	public ConstructNetworkViewTask(CySwingAppAdapter appAdapter, CyActivator activator,
			CyNetwork network, String[] userDefinedParameters, ArrayList<String> interactionTypesNamesList, CyLayoutAlgorithm layout){
		this.appAdapter = appAdapter;
		this.activator = activator;
		this.network = network;
		this.userDefinedParameters = userDefinedParameters;
		this.interactionTypesNamesList = interactionTypesNamesList;
		this.algor = layout;
	}
	
	
	@Override
	public void run(TaskMonitor taskMonitor) throws Exception {
		/**
		 * Constructs the network in the network window and applies a visual style as well
		 * as a network layout algorithm.
		 */
		final CyNetworkView networkView = appAdapter.getCyNetworkViewFactory().createNetworkView(network);
		setupVisualStyle(networkView);
		
		String attributeName = null;
		if (network.getNodeCount() > 1500 || network.getEdgeCount() > 9000){
			algor = appAdapter.getCyLayoutAlgorithmManager().getLayout("grid");
		}
		// good layouts for this include: //isom //kamada-kawai //allegro-strong-clustering (last one is an app)
		
		TaskIterator taskIterator = algor.createTaskIterator(networkView, algor.createLayoutContext(), CyLayoutAlgorithm.ALL_NODE_VIEWS, attributeName);
		taskIterator.append(new AbstractTask() {
			@Override
			public void run(TaskMonitor taskMonitor) throws Exception {
				activator.constructNetworkFinish(network, networkView, userDefinedParameters, interactionTypesNamesList);
			}
		});
		insertTasksAfterCurrentTask(taskIterator);
	}
	
	private void setupVisualStyle(CyNetworkView networkView){
		/**
		 * Sets up the visual style for the network (node shapes, labels, tooltips, colors,
		 * mappings, ...) for optimal visualization of the network.
		 */
		
		VisualStyle visualStyle = appAdapter.getVisualStyleFactory().createVisualStyle("CHATStyle");
		appAdapter.getVisualMappingManager().addVisualStyle(visualStyle);
		
		visualStyle.setDefaultValue(BasicVisualLexicon.NODE_SHAPE, NodeShapeVisualProperty.ELLIPSE);
		visualStyle.setDefaultValue(BasicVisualLexicon.NODE_LABEL_COLOR, Color.BLACK);
		visualStyle.setDefaultValue(BasicVisualLexicon.NODE_SELECTED_PAINT, new Color(61, 122, 229));
		
		VisualMappingFunctionFactory passthroughMappingFactory = appAdapter.getVisualMappingFunctionPassthroughFactory();
		PassthroughMapping<String, String> nodeLabelMapping = (PassthroughMapping<String, String>) passthroughMappingFactory.createVisualMappingFunction(ConstructNetworkTask.NODE_DISPLAY_NAME, String.class, BasicVisualLexicon.NODE_LABEL);
		visualStyle.addVisualMappingFunction(nodeLabelMapping);
		
		PassthroughMapping<String, String> nodeTooltipMapping = (PassthroughMapping<String, String>) passthroughMappingFactory.createVisualMappingFunction(CyNetwork.NAME, String.class, BasicVisualLexicon.NODE_TOOLTIP);
		visualStyle.addVisualMappingFunction(nodeTooltipMapping);
		
		PassthroughMapping<String, String> edgeTooltipMapping = (PassthroughMapping<String, String>) passthroughMappingFactory.createVisualMappingFunction(network.getDefaultEdgeTable().getColumn("interaction").toString(), String.class, BasicVisualLexicon.EDGE_TOOLTIP);
		visualStyle.addVisualMappingFunction(edgeTooltipMapping);
		
		VisualMappingFunctionFactory discreteMappingFactory = appAdapter.getVisualMappingFunctionDiscreteFactory();
		// use node shape to differentiate between contextually important nodes and other ones
		DiscreteMapping<Boolean, NodeShape> nodeShape = (DiscreteMapping<Boolean, NodeShape>) discreteMappingFactory.createVisualMappingFunction(ConstructNetworkTask.NODE_CONTEXTUALLY_IMPORTANT, Boolean.class, BasicVisualLexicon.NODE_SHAPE);
		nodeShape.putMapValue(true, NodeShapeVisualProperty.DIAMOND);
		nodeShape.putMapValue(false, NodeShapeVisualProperty.ELLIPSE);
		visualStyle.addVisualMappingFunction(nodeShape);
		
		visualStyle.setDefaultValue(BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT, Color.LIGHT_GRAY);
		
		
		visualStyle.apply(networkView);
		appAdapter.getVisualMappingManager().setVisualStyle(visualStyle, networkView);
	}
	
}
