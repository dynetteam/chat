package eu.primes.chat.internal;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * This parses the PSICQUIC registry and returns a hash map that maps database names to
 * database URLs.
 * 
 * @author Ivan Goenawan, Tanja Muetze
 */

public class PSICQUICRegistryParser {
	
	private static Map<String, UrlTuple> nameToUrlMap;  //for caching
	private static long lastDownloadTime;
	
	
	public static Map<String, UrlTuple> getPSIQUICDatabases() throws Exception{
		/**
		 * Downloads registry (information about services) from PSICQUIC server, parses
		 * information and returns a hash map that maps database names to database urls.
		 */
		
		if (nameToUrlMap == null || 
				System.currentTimeMillis() - lastDownloadTime > 300000){
			
			String registryXML = "";
			try{
				
				URL url = new URL("http://www.ebi.ac.uk/Tools/webservices/psicquic/registry/registry?action=STATUS&format=xml");
				BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
				String line;
				while((line = in.readLine()) != null) {
					registryXML += line;
				}
				in.close();
				registryXML = registryXML.replace("\n", "").replace("\r", "");
				nameToUrlMap = parseRegistryXML(registryXML);
				lastDownloadTime = System.currentTimeMillis();
				
			}catch (Exception e){
				if (nameToUrlMap == null) throw new Exception("Cannot connect to PSICQUIC registry: " + e.getMessage());
			}
		}
		
		return nameToUrlMap;
	}
	
	private static HashMap<String, UrlTuple> parseRegistryXML(String registryXML){
		/**
		 * Returns a hash map/dictionary of database names to database urls for databases
		 * that are currently active (not down) from provided services registry data.
		 **/
		
		HashMap<String, UrlTuple> nameToUrlMap = new HashMap<String, UrlTuple>();
		registryXML = registryXML.replace("<service>","\n<service>");
		String[] xmlRecords = registryXML.split("\\n");
		for (int i = 1; i < xmlRecords.length; i++){
			String record = xmlRecords[i];
			
			int startActive = record.indexOf("<active>") + 8;
			int endActive = record.indexOf("</active>");
			String active = record.substring(startActive, endActive);
			
			if (active.equals("true")){				
				int startName = record.indexOf("<name>") + 6;
				int endName = record.indexOf("</name>");
				String name = record.substring(startName, endName);
				
				int startUrl = record.indexOf("<restUrl>") + 9;
				int endUrl = record.indexOf("</restUrl>");
				String url = record.substring(startUrl, endUrl);
				
				int startOrgUrl = record.indexOf("<organizationUrl>") + 17;
				int endOrgUrl = record.indexOf("</organizationUrl>");
				String orgUrl = record.substring(startOrgUrl, endOrgUrl);
				
				nameToUrlMap.put(name, new UrlTuple(url, orgUrl));	
			}
		}
		
		//for debugging
		//nameToUrlMap.put("debugging", new UrlTuple("http://localhost:9090/psicquic/webservices/current/search/", "test.com"));
		
		return nameToUrlMap;
	}
	
	protected static class UrlTuple{
		/**
		 * Class to store URLs for REST calls and for accessing the organization's website.
		 */
		private String restUrl;
		private String organizationUrl;
		
		protected UrlTuple(String restUrl, String organizationUrl){
			this.restUrl = restUrl;
			this.organizationUrl = organizationUrl;
		}
		protected String getRestUrl(){
			return this.restUrl;
		}
		protected String getOrganizationUrl(){
			return this.organizationUrl;
		}
	}
}
