package eu.primes.chat.internal;

/**
 * This allows to access edge data.
 * 
 * @author Ivan Goenawan
 */

public class Edge {
	private StringBuilder detectionMethod;
	private StringBuilder firstAuthor;
	private StringBuilder publicationId;
	private StringBuilder interactionType;
	private StringBuilder sourceDatabases;
	private StringBuilder interactionId;
	private StringBuilder confidenceScore;
	
	
	public Edge(String interactionId, String interactionType, String detectionMethod, String confidenceScore, 
			String firstAuthor, String publicationId, String sourceDatabase){
		
		this.interactionId = new StringBuilder(interactionId);
		this.interactionType = new StringBuilder(interactionType);
		this.detectionMethod = new StringBuilder(detectionMethod);
		this.confidenceScore = new StringBuilder(confidenceScore);
		this.firstAuthor = new StringBuilder(firstAuthor);
		this.publicationId = new StringBuilder(publicationId);
		this.sourceDatabases = new StringBuilder(sourceDatabase);
	}
	
	
	
	public String getDetectionMethod() {
		return detectionMethod.toString();
	}

	public String getFirstAuthor() {
		return firstAuthor.toString();
	}

	public String getPublicationId() {
		return publicationId.toString();
	}

	public String getInteractionType() {
		return interactionType.toString();
	}

	public String getSourceDatabases() {
		return sourceDatabases.toString();
	}

	public String getInteractionId() {
		return interactionId.toString();
	}

	public String getConfidenceScore() {
		return confidenceScore.toString();
	}
}
