package eu.primes.chat.internal;

import org.cytoscape.app.swing.CySwingAppAdapter;
import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;

/**
 * This is the task factory that makes the initial dialog box appear.
 * 
 * @author Ivan Goenawan
 */

public class ShowInitDialogTaskFactory extends AbstractTaskFactory {

	private CySwingAppAdapter appAdapter;
	private CyActivator activator;
	
	public ShowInitDialogTaskFactory(CySwingAppAdapter appAdapter, CyActivator activator){
		this.appAdapter = appAdapter;
		this.activator = activator;
	}
	
	
	@Override
	public TaskIterator createTaskIterator() {
		return new TaskIterator(new ShowInitDialogTask(appAdapter, activator));
	}

}
