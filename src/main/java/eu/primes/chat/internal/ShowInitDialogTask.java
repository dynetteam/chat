package eu.primes.chat.internal;

import org.cytoscape.app.swing.CySwingAppAdapter;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

/**
 * This is the task which launches the initial dialog box.
 * 
 * @author Ivan Goenawan
 */

public class ShowInitDialogTask extends AbstractTask {

	private CySwingAppAdapter appAdapter;
	private CyActivator activator;
	
	
	public ShowInitDialogTask(CySwingAppAdapter appAdapter, CyActivator activator){
		this.appAdapter = appAdapter;
		this.activator = activator;
	}
	
	@Override
	public void run(TaskMonitor taskMonitor) throws Exception {
		taskMonitor.setTitle("CHAT");
		taskMonitor.setStatusMessage("Downloading PSICQUIC data...");
		
		InitDialog initDialog = new InitDialog(appAdapter, activator);
	}

}
